import {UIRouter} from "ui-router-ng2/router";
import {INITIAL_STATES} from "../src/app/app.states";
import {Http} from '@angular/http';
import { Injectable, Injector } from '@angular/core';

@Injectable()
export class MyUIRouterConfig {
    constructor(private injector: Injector) {}

    configure(uiRouter: UIRouter) {
        INITIAL_STATES.forEach(state => uiRouter.stateRegistry.register(state));
        uiRouter.urlRouterProvider.otherwise(() => uiRouter.stateService.go("app.calculator", null, null));
        let rootState = uiRouter.stateRegistry.root();
        rootState.resolve['http'] = () => this.injector.get(Http);
    }
}