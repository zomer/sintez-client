import { AppComponent } from '../app/app.component';
import {Ng2StateDeclaration} from "ui-router-ng2/ng2/interface";
import { CALCULATOR_STATES } from '../calculator/calculator.states';
import { LOGS_STATES } from '../logs/logs.states';

// The top level states
let MAIN_STATES: Ng2StateDeclaration[] = [
    { name: 'app', component: AppComponent }
];

export let INITIAL_STATES: Ng2StateDeclaration[] = MAIN_STATES
    .concat(CALCULATOR_STATES)
    .concat(LOGS_STATES);