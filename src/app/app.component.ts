import { Component } from '@angular/core';
import {UIROUTER_DIRECTIVES} from "ui-router-ng2";

@Component({
  directives: [UIROUTER_DIRECTIVES],
  selector: 'my-app',
  template: require('./app.component.html')
})
export class AppComponent {
}

