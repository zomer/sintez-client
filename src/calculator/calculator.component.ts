import {Component, Injectable} from '@angular/core';
import {FieldValidator} from './calculatorFieldValidator.ts'
import 'rxjs/add/operator/map'
import { LogService } from '../logs/logs.service';

import {
    FormBuilder,
    Control,
    ControlGroup,
    Validators
} from '@angular/common';


@Component({
    selector: 'app-calculator',
    template: require('./calculator.component.html')
})

export class CalculatorComponent {
    form: ControlGroup;
    leftOperand: Control;
    rightOperand: Control;
    result: string;
    isReset: boolean;

    constructor(private builder:FormBuilder,
                private logService: LogService) {

        this.form = builder.group({
            'leftOperand': ["", Validators.compose([Validators.required, FieldValidator.onlyNumber])],
            'rightOperand': ["", Validators.compose([Validators.required, FieldValidator.onlyNumber])]
        });
    }

    ngAfterViewInit() {
        this.form.valueChanges.subscribe(
            (form: any) => {
                this.isReset = false;
            });
    }

    submitResult(operator, value) {
        var leftOperand = Number(value.leftOperand);
        var rightOperand = Number(value.rightOperand);
        var result;
        var arithmeticType;

        if (operator === "+") {
            result = leftOperand + rightOperand;
            arithmeticType = "ADDITION";
        } else if (operator === "-") {
            result = leftOperand - rightOperand;
            arithmeticType = "SUBTRACTION";
        } else if (operator === "*") {
            result = leftOperand * rightOperand;
            arithmeticType = "MULTIPLICATION";
        } else if (operator === "/") {
            result = leftOperand / rightOperand;
            arithmeticType = "DIVISION";
        }

        this.result = result;

        this.logService.addLog(leftOperand, rightOperand, arithmeticType, result);
    }

    cleanData() {
        this.result = "";
        for (let name in this.form.controls) {
            (<Control>this.form.controls[name]).updateValue("");
            this.form.controls[name].setErrors(null);
        }

        this.isReset = true;
    }

    isValid() {
        return !this.form.valid || this.isReset;
    }
}



