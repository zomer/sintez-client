import { Control } from "@angular/common";

interface ValidationResult {
    [key:string]:boolean;
}
export class FieldValidator {

    static onlyNumber(control: Control): ValidationResult {

        var NUMBER_REGEXP = /^\d+(?:\.\d+|)$/;

        if (control.value !=="" && !NUMBER_REGEXP.test(control.value)) {
            return {"invalidNumber": true };
        }

        return null;
    }
}