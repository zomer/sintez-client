import {CalculatorComponent} from "./calculator.component";

export let CALCULATOR_STATES = [
    { name: 'app.calculator', url: '/calculator', views: {  $default: {component: CalculatorComponent }}},
];