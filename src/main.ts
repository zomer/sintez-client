import { bootstrap } from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from "@angular/http";
import { UIROUTER_PROVIDERS, UiView, UIRouterConfig } from "ui-router-ng2";
import { MyUIRouterConfig } from "../config/routes";
import { provide } from "@angular/core";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { LocationStrategy, PathLocationStrategy, PlatformLocation } from "@angular/common";
import { BrowserPlatformLocation } from '@angular/platform-browser';
import { FORM_PROVIDERS } from '@angular/common';
import { JSONP_PROVIDERS } from '@angular/http';
import { LogService } from '../src/logs/logs.service';


bootstrap(UiView, [
    provide(LocationStrategy, { useClass: PathLocationStrategy }),
    provide(PlatformLocation, { useClass: BrowserPlatformLocation }),
    UIROUTER_PROVIDERS,
    HTTP_PROVIDERS,
    FORM_PROVIDERS,
    JSONP_PROVIDERS,
    LogService,
    // Provide a custom UIRouterConfig to configure UI-Router
    provide(UIRouterConfig, { useClass: MyUIRouterConfig })
]);
