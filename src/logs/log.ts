export class Log {
    id:number;
    leftOperand:number;
    rightOperand:number;
    result:number;
    arithmeticType:String;
}
