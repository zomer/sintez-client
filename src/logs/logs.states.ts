import {LogsComponent} from "./logs.component";


export let LOGS_STATES = [
    { name: 'app.logs', url: '/logs', component: LogsComponent },
];