import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class LogService {

    private addUrl = 'http://localhost:8181/logs/add';
    private logsUrl = 'http://localhost:8181/logs/list';
    
    constructor(private http:Http) {
    }

    addLog (leftOperand, rightOperand, arithmeticType, result) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        this.http.post(this.addUrl, JSON.stringify({leftOperand: leftOperand, rightOperand: rightOperand, arithmeticType: arithmeticType, result: result}), options)
            .subscribe(
                err => console.error('There was an error: ' + err)
            );
    }

    getLogs() {
        return this.http.get(this.logsUrl);
    }
}