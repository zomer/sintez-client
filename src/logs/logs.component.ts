import {Component} from '@angular/core';
import { Injectable } from '@angular/core';
import { Log } from './log';
import {LogService} from "./logs.service";

@Component({
    selector: 'app-logs',
    template: require('./logs.html')
})

@Injectable()
export class LogsComponent {
    
    logs: Log[];

    constructor(private logService: LogService) {
        this.logService.getLogs().subscribe(data => this.logs = data.json(),
            err =>  console.error('There was an error: ' + err));
    }
}
